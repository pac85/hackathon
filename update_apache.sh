sudo systemctl stop httpd

sudo rm -rf /srv/http
sudo cp -r ./ /srv/http

#replaces tab with spaces to make python happy
sudo find /srv/http/python -name '*.py' -exec sed -i.orig 's/\t/    /g' {} +

sudo chmod -R 777 /srv/http/

sudo cp httpd.conf /etc/httpd/conf/httpd.conf

sudo systemctl start httpd
echo 'Done!'
