-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: voli
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Cibo`
--

DROP TABLE IF EXISTS `Cibo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cibo` (
  `ID` int(32) NOT NULL AUTO_INCREMENT,
  `Primo piatto` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Secondo piatto` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Dessert` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Bibita` varchar(32) CHARACTER SET utf8 NOT NULL,
  `stelle` int(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cibo`
--

LOCK TABLES `Cibo` WRITE;
/*!40000 ALTER TABLE `Cibo` DISABLE KEYS */;
INSERT INTO `Cibo` VALUES (1,'Pasta with tomato sauce','meat and fries potatoes','cheesecake','acqua',3),(2,'gnocchi with pesto','chicken and baked potatoes','chocolate cookies','coca cola',3),(3,'Couscous with vegetables','Grilled vegetables','Light soufflé with pears','Orange juice',3),(4,'Spaghetti carbonara','Grilled meat','Crepes with nutella','Sprite',4),(5,'Lasagne','Caesar salad','Brownies','Beer',4),(6,'Risotto with mushrooms','Beef steak','Muffin','Acqua',4),(7,'Spaghetti with mussels and clams','Swordfish','Cherry tart','White wine',5),(8,'Sea rice','Fried squid','Donuts','White wine',5),(9,'Sushi','Baked crayfish','Crème brùlèe','Champagne',5);
/*!40000 ALTER TABLE `Cibo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Hotel`
--

DROP TABLE IF EXISTS `Hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hotel` (
  `ID` int(40) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Indirizzo` varchar(30) CHARACTER SET utf8 NOT NULL,
  `CAP` int(32) NOT NULL,
  `Citta` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `Tipo struttura` varchar(40) CHARACTER SET utf8 NOT NULL,
  `Numero telefono` varchar(90) CHARACTER SET utf8 NOT NULL,
  `NumeroStelle` int(10) NOT NULL,
  `Immagine` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Hotel`
--

LOCK TABLES `Hotel` WRITE;
/*!40000 ALTER TABLE `Hotel` DISABLE KEYS */;
INSERT INTO `Hotel` VALUES (1,'Red Wing','32st avenue',56894,'Londra','Poooppo@live.it','B&B','3298855624',3,'1.png'),(2,'Red wine','67 uccino',96472,'Londra','winered@gmail.com','Hotel','3298874584',4,'2.png'),(3,'Mela Blu','Via Roma 1',66695,'Madrid','MelaBlu@gmail.com','B&B','7895462155',3,'3.png'),(4,'Backing Hotel','25st Street',89456,'Londra','BackingHotel@gmail.com','Hotel','3264587927',5,'4.png'),(5,'London Hotels company','26st Street',22564,'London','LondonHotelcompany@gmail.com','Hotel','3279564524',5,'5.png'),(6,'Lady\'s Hotel','96st Street',15634,'Londra','LadyHotel@libero.com','Hotel','3654895624',3,'6.png'),(7,'Scoglio d\'oro','98 Europa',45479,'Madrid','Scoglio@gmail.com','B&D','3654895626',2,'7.png'),(8,'Padding and co.','22 avenue',93723,'Londra','Padding@gmail.com','Hotel','3298751658',1,'8.png'),(9,'Hotel Stars','Via Fratelli Inglesi,89',93724,'Madrid','HotelStars@gmail.com','Hotel','3245896246',4,'9.png'),(10,'Bom Bom Hotel','Corso dei Duemila',93726,'Londra','BomBomHotel@gmail.com','Hotel','8954755741',5,'10.png'),(11,'tourist hotel','via Inglese',95644,'Palermo','touristhotel@gmail.com','Hotel','9564658854',3,'11.png'),(12,'Devil Hote','via Infernale',25631,'Madrid','DevilHote@gmail.com','Hotel','3268597484',1,'12.png'),(13,'Ertyuc ','weyej 23',93736,'Madrid ','Ertyuc@gmail.com','B&B','5487984695',5,'13.png'),(14,'Chicos Hotel','Pwet e 76',77562,'Madrid','chicos@gmail.com','Hotel','3298751485',2,'14.png'),(15,'Op and po','69st avenue park',65984,'Madrid','Op@gmail.com','Hotel','3294586214',5,'15.png'),(16,' Gondola','Via chsj,43',14456,'Londra','Gondoliere@gmail.com','Hotel','3247895624',4,'16.png'),(17,'Dino\'s Postro','Corso Primordiale,22',89654,'Londra','dino@gmail.com','Hotel','3297895641',3,'17.png'),(18,'Wertrf Hotel','jungle',45698,'Madrid','wertrf@gmail.com','Hotel','3296547892',4,'18.png'),(19,'Bikini bottom','sky street',23659,'Londra','bikini@gmail.com','B&B','3295687421',3,'19.png'),(20,'Pooarty Hotel','Via dei gondolai, 46',22013,'Londra','pooarty@gmail.com','Hotel','7218777773',5,'20.png'),(21,'Yulu','22 st ring',96578,'Londra','yulu@gmail.com','Hotel','3295687452',5,'21.png'),(22,'Opher','viale croissan',79854,'Madrid','opher@gmail.com','B&B','3295482655',4,'22.png'),(23,'Night hotel','mulen rouge',79562,'Londra','nighthotel@gmail.com','Hotel','3295684521',3,'23.png'),(24,'Mincraft','strada quadrata',11456,'Londra','creeper@gmail.com','Hotel','3272584966',4,'24.png'),(25,'Rolly stones','simpaty for the devil',20663,'Madrid','Stones@gmail.com','Hotel','3264589657',3,'25.png');
/*!40000 ALTER TABLE `Hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Mezzi`
--

DROP TABLE IF EXISTS `Mezzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mezzi` (
  `ID` int(32) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Colore` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Nome` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Carburante` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Modello` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Marca` varchar(32) CHARACTER SET utf8 NOT NULL,
  `Numero porte` int(32) NOT NULL,
  `Stelle` int(32) NOT NULL,
  `Citta` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Mezzi`
--

LOCK TABLES `Mezzi` WRITE;
/*!40000 ALTER TABLE `Mezzi` DISABLE KEYS */;
INSERT INTO `Mezzi` VALUES (1,'Car','Black','Peugeot 3008','Diesel','3008','Peugeot',5,4,'London'),(2,'Car','Red','Mercedes \"C\" Class','Diesel','C','Mercedes',5,5,'London'),(3,'Car','Bordeaux','BMW Serie 2 Active Tourer','Diesel',' Serie 2 Active Tourer','BMW',6,4,'London'),(4,'Car','Grey','Mercedes \"B\" Class','Diesel','B','Mercedes',5,4,'London'),(5,'Car','Blue','Fiat Tipo Station Wagon','Diesel','Station Wagon','Fiat',5,3,'London'),(6,'Car','White','Dacia Logan MCV','Petrol',' Logan MCV','Dacia ',4,2,'London'),(7,'Car','Purple','Citroen C4 Grand Picasso','Diesel',' C4 Grand Picasso','Citroen ',6,4,'London'),(8,'Car','Magenta','Kia Carens','Diesel',' Carens','Kia',3,4,'London'),(9,'Car','Grey','BMW M3','Diesel',' M3','BMW ',5,5,'London'),(10,'Car','Silver','Jaguar XF-S','Petrol','XF-S',' XF-S',3,5,'London'),(11,'Car','Lime','McLaren 720S','Diesel','720S','McLaren',3,5,'Madrid'),(12,'Car','Tan','Audi S1 2.0 TFSI quattro','Diesel',' S1 2.0 TFSI quattro','Audi',3,4,'Madrid'),(13,'Car','Navy','Range Rover Sport SVR','Diesel','Sport SVR','Range Rover',6,4,'Madrid'),(14,'Car','Red','Alfa Romeo Stelvio Quadrifoglio','Diesel',' Stelvio Quadrifoglio','Alfa Romeo ',6,5,'Madrid'),(15,'Car','White','Renault Zoe','Electric',' Zoe','Renault ',3,4,'Madrid'),(16,'Car','Indigo','Tesla Roadster','Electric',' Roadster','Tesla ',3,5,'Madrid'),(17,'Car','Black','Nissan Leaf','Electric','Leaf','Nissan ',6,4,'Madrid'),(18,'Motor Bike','Grey','BMW F850 GS','Diesel','F850 GS','BMW',0,4,'London'),(19,'Motor Bike','Orange','KTM 790 DUKE','Diesel','790 DUKE','KTM',0,3,'London'),(20,'Motor Bike','Black','HONDA CB1000R','Petrol','CB1000R','HONDA ',0,4,'Madrid'),(21,'Motor Bike','White','Honda SH 2017i','Petrol','SH 2017i','Honda',0,2,'Madrid'),(22,'Boat','White','Mostes Offshore 31','Diesel','Offshore 31','Mostes ',0,4,'London'),(23,'Boat','Blue','Tullio Abbate 20 Elite Motoscafo','Diesel','20 Elite Motoscafo','Tullio Abbate',0,3,'Madrid'),(24,'Boat','Black','Bavaria Motor Yacht 37 Sport','Diesel','Motor Yacht 37 Sport','Bavaria ',0,5,'London'),(25,'Boat','Green','GANZ BOATS OVATION 6.8 ','Diesel','BOATS OVATION 6.8 ','BOATS OVATION 6.8 ',0,4,'Madrid'),(26,'Bike','Green','Beflex LFQ2','Electric','LFQ2','Beflex',0,4,'London'),(27,'Bike','Yellow','Be monster LFD2','Electric','LFD2','Be monster',0,5,'London'),(28,'Bike','Black','Mtb Kona 29','No','Kona 29','Mtb',0,3,'Madrid'),(29,'Bike','White','Elios Futura','Electric','Futura','Elios',0,5,'Madrid');
/*!40000 ALTER TABLE `Mezzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(30) CHARACTER SET utf8 NOT NULL,
  `e_mail` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'Vito Porcoddio','Puleio','vitopuleio1999@gmail.com','Ciao12345');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserTaste`
--

DROP TABLE IF EXISTS `UserTaste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserTaste` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `MoF` tinyint(1) NOT NULL,
  `AgeRange` int(11) NOT NULL,
  `Motivation` int(11) NOT NULL,
  `FreeTime` tinyint(1) NOT NULL,
  `MoRoS` int(11) NOT NULL,
  `Ativity` int(11) NOT NULL,
  `Compagnon` int(11) NOT NULL,
  `Mean` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserTaste`
--

LOCK TABLES `UserTaste` WRITE;
/*!40000 ALTER TABLE `UserTaste` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserTaste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flightSegment`
--

DROP TABLE IF EXISTS `flightSegment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flightSegment` (
  `offerID` varchar(50) CHARACTER SET utf8 NOT NULL,
  `currency` varchar(10) CHARACTER SET utf8 NOT NULL,
  `totalPrice` double NOT NULL,
  `segmentID` varchar(30) CHARACTER SET utf8 NOT NULL,
  `ODRef` varchar(10) CHARACTER SET utf8 NOT NULL,
  `departure_airportCode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `departure_time` varchar(10) CHARACTER SET utf8 NOT NULL,
  `departure_airportName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `departure_terminalName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `departure_date` varchar(20) CHARACTER SET utf8 NOT NULL,
  `arrival_airportCode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `arrival_time` varchar(10) CHARACTER SET utf8 NOT NULL,
  `arrival_airportName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `arrival_terminalName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `arrival_date` varchar(20) CHARACTER SET utf8 NOT NULL,
  `airlineID` varchar(11) CHARACTER SET utf8 NOT NULL,
  `marketing_name` varchar(11) CHARACTER SET utf8 NOT NULL,
  `flightNumber` varchar(11) CHARACTER SET utf8 NOT NULL,
  `aircraftCode` varchar(11) CHARACTER SET utf8 NOT NULL,
  `aircraft_name` varchar(11) CHARACTER SET utf8 NOT NULL,
  `flightDuration` varchar(11) CHARACTER SET utf8 NOT NULL,
  `class_refs` varchar(11) CHARACTER SET utf8 NOT NULL,
  `class_code` varchar(11) CHARACTER SET utf8 NOT NULL,
  `class_marketingName` varchar(11) CHARACTER SET utf8 NOT NULL,
  `class_disclosures` varchar(11) CHARACTER SET utf8 NOT NULL,
  `seatsLeft` varchar(11) CHARACTER SET utf8 NOT NULL,
  `stopQuantity` varchar(11) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flightSegment`
--

LOCK TABLES `flightSegment` WRITE;
/*!40000 ALTER TABLE `flightSegment` DISABLE KEYS */;
/*!40000 ALTER TABLE `flightSegment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packetItems`
--

DROP TABLE IF EXISTS `packetItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packetItems` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pHeadID` int(11) NOT NULL,
  `itemType` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemID` varchar(50) CHARACTER SET utf8 NOT NULL,
  `itemIDint` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packetItems`
--

LOCK TABLES `packetItems` WRITE;
/*!40000 ALTER TABLE `packetItems` DISABLE KEYS */;
INSERT INTO `packetItems` VALUES (9,16,'0','OFFER-mwv0YxzK0MH0HcbT1gZCWSywcJytKsB3SybRGCVHLMW5',0,0),(10,16,'2','1_food',0,0),(11,17,'2','5_hotel',0,0),(12,17,'2','2_food',0,0),(13,17,'2','5_mean',0,0),(14,18,'2','2_food',0,0),(15,19,'2','5_hotel',0,0),(16,20,'2','11_hotel',0,0),(17,20,'2','5_mean',0,0),(18,20,'2','5_hotel',0,0),(19,20,'2','9_food',0,0),(20,21,'2','11_hotel',0,0),(21,21,'2','5_mean',0,0),(22,21,'2','5_hotel',0,0),(23,21,'2','9_food',0,0),(24,22,'2','11_hotel',0,0),(25,22,'2','5_mean',0,0),(26,22,'2','5_hotel',0,0),(27,22,'2','9_food',0,0);
/*!40000 ALTER TABLE `packetItems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packetsHead`
--

DROP TABLE IF EXISTS `packetsHead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packetsHead` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packetsHead`
--

LOCK TABLES `packetsHead` WRITE;
/*!40000 ALTER TABLE `packetsHead` DISABLE KEYS */;
INSERT INTO `packetsHead` VALUES (16,'testp',0),(17,'tewst',0),(18,'',0),(19,'test',0),(20,'Trip to london',0),(21,'Trip to london',0),(22,'Trip to london',0);
/*!40000 ALTER TABLE `packetsHead` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-16 23:19:01
