/*function flightsearch()
{
    var hotelxmlhttp = new hotelxmlhttpRequest();
    hotelxmlhttp.onreadystatechange = function() {
        if (hotelxmlhttp.readyState == 4 && hotelxmlhttp.status == 200)
        {
            var restext = hotelxmlhttp.resposneText;
            console.log(restext);
            document.getElementById("fsearchresults").innerHTML = restext;
        }
    }
    hotelxmlhttp.open("GET", "/flightSearch", true); // true for asynchronous
    hotelxmlhttp.send(null);

}*/
var stars = 3;

var hotelxmlhttp = new XMLHttpRequest();
function hotelsearch()
{
    var requestBodyObj = new Object();

    requestBodyObj.city = document.getElementById("hotelcity").value;
    requestBodyObj.stars = stars;

    var element = document.getElementById("hsearchresults");
    element.innerHTML = '<p><center><img src="loading.gif"></img></center></p>';
    hotelxmlhttp.open("POST", "/hotelSearch");
    hotelxmlhttp.setRequestHeader("Content-type", "text/json");
    hotelxmlhttp.onreadystatechange = function()
    {
        if (hotelxmlhttp.readyState == 4 && hotelxmlhttp.status == 200)
        {
            element.innerHTML = hotelxmlhttp.responseText;
        }
    }
    hotelxmlhttp.send(JSON.stringify(requestBodyObj));
}
