var is_oneway = true;

function slected_oneway()
{
    is_oneway = true;
    document.getElementById("oneway").style.display = "block";
    document.getElementById("roundtrip").style.display = "none";
}

function slected_roundtrip()
{
    is_oneway = false;
    document.getElementById("roundtrip").style.display = "block";
    document.getElementById("oneway").style.display = "block";
}

var xmlhttp = new XMLHttpRequest();
function flightsearch()
{
    var requestBodyObj = new Object();

    requestBodyObj.is_oneway = is_oneway;
    requestBodyObj.oneway_departure = document.getElementById("oneway_departure").value;
    requestBodyObj.oneway_arrival = document.getElementById("oneway_arrival").value;
    if(!is_oneway)
    {
        requestBodyObj.roundtrip_departure = document.getElementById("roundtrip_departure").value;
        requestBodyObj.roundtrip_arrival = document.getElementById("roundtrip_arrival").value;
    }

    var element = document.getElementById("fsearchresults");
    element.innerHTML = '<p><center><img src="loading.gif"></img></center></p>';
    xmlhttp.open("POST", "/flightSearch");
    xmlhttp.setRequestHeader("Content-type", "text/json");
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            element.innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.send(JSON.stringify(requestBodyObj));
}
