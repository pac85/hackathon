var FLIGHT = 0;
var HOTEL = 1;
var RENT = 2;
var OTHER = 3;
var PACKET = 4;
var FOOD = 5;

var CartElements = new Array();

var userID = 0;

function addFlightToCart_pre(offerID)
{
    addFlightToCart(offerID, parseInt(document.getElementById(offerID+"_qty").value), document.getElementById(offerID).innerHTML);
}

function addFlightToCart(offerID, qty, innerHTML)
{
    //console.log("adding" + offerID + " to cart " + qty + innerHTML);
    var to_add = new Object();
    to_add.id = CartElements.length;
    to_add.offerID = offerID;
    to_add.qty = qty;
    to_add.item_type = FLIGHT;
    to_add.html = innerHTML;

    CartElements.push(to_add);

    //sort by type, this ensures that
    //items of the same type will be close togheter
    CartElements.sort(
        function(a, b)
        {
            return a.item_type > b.item_type;
        });
}

function get_cart_html()
{
    var result = "<h6 class='w3-text-gray'>"+CartElements.length+" items </h6>";
    for(var i = 0;i < CartElements.length;i++)
    {
        result += CartElements[i].html;
    }
    return result;
}

function updateCart()
{
    document.getElementById("cartres").innerHTML = get_cart_html()+document.getElementById("CartFooter").innerHTML;
}

function updateFlightQty(offerID)
{
    var new_qty = parseInt(document.getElementById(offerID+"_cart_qty").value);
    CartElements.find(
        function(flight)
        {
            return flight.offerID == offerID;
        }).qty = new_qty;
    updateCart();
}

function removeFlight(offerID)
{
    var tor_index = CartElements.indexOf(
        function(flight)
        {
            return flight.offerID == offerID;
        });

    CartElements.splice(tor_index-1, 1);
    updateCart();
}

function addMeanToCart_pre(offerID)
{
    addMeanToCart(offerID, parseInt(document.getElementById(offerID+"_qty").value), document.getElementById(offerID).innerHTML);
}

function addMeanToCart(offerID, qty, innerHTML)
{
    var to_add = new Object();
    to_add.id = CartElements.length;
    to_add.offerID = offerID;
    to_add.qty = qty;
    to_add.item_type = RENT;
    to_add.html = innerHTML;

    CartElements.push(to_add);

    //sort by type, this ensures that
    //items of the same type will be close togheter
    CartElements.sort(
        function(a, b)
        {
            return a.item_type > b.item_type;
        });
}

function addFoodToCart_pre(offerID)
{
    addMeanToCart(offerID, parseInt(document.getElementById(offerID+"_qty").value), document.getElementById(offerID).innerHTML);
}

function addFoodToCart(offerID, qty, innerHTML)
{
    var to_add = new Object();
    to_add.id = CartElements.length;
    to_add.offerID = offerID;
    to_add.qty = qty;
    to_add.item_type = FOOD;
    to_add.html = innerHTML;

    CartElements.push(to_add);

    //sort by type, this ensures that
    //items of the same type will be close togheter
    CartElements.sort(
        function(a, b)
        {
            return a.item_type > b.item_type;
        });
}

function addHotelToCart_pre(offerID)
{
    addMeanToCart(offerID, parseInt(document.getElementById(offerID+"_qty").value), document.getElementById(offerID).innerHTML);
}

function addHotelToCart(offerID, qty, innerHTML)
{
    var to_add = new Object();
    to_add.id = CartElements.length;
    to_add.offerID = offerID;
    to_add.qty = qty;
    to_add.item_type = FOOD;
    to_add.html = innerHTML;

    CartElements.push(to_add);

    //sort by type, this ensures that
    //items of the same type will be close togheter
    CartElements.sort(
        function(a, b)
        {
            return a.item_type > b.item_type;
        });
}


function updateItemQty(offerID)
{
    var new_qty = parseInt(document.getElementById(offerID+"_cart_qty").value);
    CartElements.find(
        function(flight)
        {
            return flight.offerID == offerID;
        }).qty = new_qty;
    updateCart();
}

function removeItem(offerID)
{
    var tor_index = CartElements.indexOf(
        function(flight)
        {
            return flight.offerID == offerID;
        });

    CartElements.splice(tor_index-1, 1);
    updateCart();
}

var packetxmlhttp = new XMLHttpRequest();
function saveCart(name)
{
    var requestObject = new Object();
    requestObject.name = name;
    requestObject.userID = userID;
    requestObject.cart = CartElements;
    var element = document.getElementById("Cart-footer");
    element.innerHTML = '<p><center><img src="loading.gif"></img></center></p>';
    packetxmlhttp.open("POST", "/addPacket");
    packetxmlhttp.setRequestHeader("Content-type", "text/json");
    packetxmlhttp.onreadystatechange = function()
    {
        if (packetxmlhttp.readyState == 4 && packetxmlhttp.status == 200)
        {
            element.innerHTML = packetxmlhttp.responseText;
        }
    }
    packetxmlhttp.send(JSON.stringify(requestObject));
}

var packetsearchxmlhttp = new XMLHttpRequest();
function searchPacket()
{
    var element = document.getElementById("psres");
    element.innerHTML = '<p><center><img src="loading.gif"></img></center></p>';
    packetxmlhttp.open("POST", "/searchPacket");
    packetxmlhttp.setRequestHeader("Content-type", "text/json");
    packetxmlhttp.onreadystatechange = function()
    {
        if (packetxmlhttp.readyState == 4 && packetxmlhttp.status == 200)
        {
            element.innerHTML = packetxmlhttp.responseText;
        }
    }
    packetxmlhttp.send(JSON.stringify(CartElements));
}

var foodsearchxmlhttp = new XMLHttpRequest();
function searchFood()
{
    var element = document.getElementById("FoodRes");
    element.innerHTML = '<p><center><img src="loading.gif"></img></center></p>';
    foodsearchxmlhttp.open("POST", "/searchFood");
    foodsearchxmlhttp.setRequestHeader("Content-type", "text/json");
    foodsearchxmlhttp.onreadystatechange = function()
    {
        if (foodsearchxmlhttp.readyState == 4 && foodsearchxmlhttp.status == 200)
        {
            element.innerHTML = foodsearchxmlhttp.responseText;
        }
    }
    foodsearchxmlhttp.send(JSON.stringify(CartElements));
}

/*
var hotelxmlhttp = new XMLHttpRequest();
function hotelsearch()
{
    var requestBodyObj = new Object();

    requestBodyObj.city = document.getElementById("hotelcity").value;
    requestBodyObj.stars = 5;

    var element = document.getElementById("hsearchresults");
    element.innerHTML = '<p><center><img src="loading.gif"></img></center></p>';
    hotelxmlhttp.open("POST", "/hotelSearch");
    hotelxmlhttp.setRequestHeader("Content-type", "text/json");
    hotelxmlhttp.onreadystatechange = function()
    {
        if (hotelxmlhttp.readyState == 4 && hotelxmlhttp.status == 200)
        {
            element.innerHTML = hotelxmlhttp.responseText;
        }
    }
    hotelxmlhttp.send(JSON.stringify(requestBodyObj));
}*/
