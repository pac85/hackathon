import pymysql.cursors
import json

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='vito',
                             db='voli',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

class Food:
    def __init__(self,
    ID,
    PrimiPiatti,
    SecondoPiatto,
    Dessert,
    Bibita,
    stelle
    ):
        self.ID = ID
        self.PrimiPiatti = PrimiPiatti
        self.SecondoPiatto = SecondoPiatto
        self.Dessert = Dessert
        self.Bibita = Bibita
        self.stelle = stelle

    def get_partial_page(self):
        return """
        <div id="""+"'"+str(self.ID)+"_food'"+""" style="display:none">
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+'"foodImages/'+str(self.ID)+'.png"'+"""style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.PrimiPiatti +"""|</b> </h3>
                    </th>
                    <th>
                        <input id="""+"\""+str(str(self.ID))+"_food_cart_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                        <button class="w3-button w3-dark-grey" onclick="updateItemQty("""+"'"+str(self.ID)+"_food'"+""");" >update</button>
                        </br>
                        <button class="w3-button w3-dark-grey" onclick="removeItem("""+"'"+str(self.ID)+"_food'"+""");" >X</button>
                    </th>
                </tr>
            </table>
        </div>
        <div>
            <table style="width:100%">
              <tr>
                <th style="width:5%"><img src="""+'"foodImages/'+str(self.ID)+'.png"'+"""style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                <th style="width:90%">
                    <h3><b style="color:#E65940">|"""+ self.PrimiPiatti +"""|</b> </h3>
                </th>
                <th>
                    <input id="""+"\""+str(self.ID)+"_food_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                    </br>
                    <button class="w3-button w3-dark-grey" onclick="addFoodToCart_pre("""+"'"+str(self.ID)+"_food'"+""");" >add to cart</button>
                </th>
              </tr>
            </table>
        </div>
        """

def pull_food():
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `Cibo` WHERE 1"
        cursor.execute(sql)

        foods = []
        for food in cursor.fetchall():
            foods.append(Food(
            food['ID'],
            food['Primo piatto'],
            food['Secondo piatto'],
            food['Dessert'],
            food['Bibita'],
            food['stelle'],
            ))
        return foods

def get_packets_result():
    result = ''
    for food in pull_food():
        result += food.get_partial_page()
    return result.encode('utf-8')
