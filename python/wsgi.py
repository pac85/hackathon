#-*- coding: utf-8 -*-
from urllib.request import Request, urlopen

work_dir = '/srv/http/python/'

def send_request(xml_data, target):
    req =  Request(target, data = xml_data.encode('utf8'), headers = {'Content-Type' : 'text/xml'}, method = 'POST')
    return urlopen(req).read()

url_str = 'http://live3-wsd-dev.crsengine.com/connectorxml/services'

xml_request = open(work_dir + 'request.xml', 'r').read()

def wsgi_app(environ, start_response):
    import sys
    test = send_request(xml_request, url_str)
    status = '200 OK'
    headers = [('Content-type', 'text/xml'),
               ('Content-Length', str(len(test)))]
    start_response(status, headers)
    yield test

# mod_wsgi need the *application* variable to serve our small app
application = wsgi_app
