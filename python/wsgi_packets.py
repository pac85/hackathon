#-*- coding: utf-8 -*-
import os
os.chdir("/srv/http/python")
import sys

path = '/srv/http/python'
if path not in sys.path:
   sys.path.insert(0, path)

print(os.getcwd())
from packets import *
from urllib.request import Request, urlopen

work_dir = '/srv/http/python/'

def wsgi_app(environ, start_response):
    import sys

    # the environment variable CONTENT_LENGTH may be empty or missing
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    # When the method is POST the variable will be sent
    # in the HTTP request body which is passed by the WSGI server
    # in the file like wsgi.input environment variable.
    request_body = environ['wsgi.input'].read(request_body_size)

    test = add_packet(request_body.decode('utf-8'))
    status = '200 OK'
    headers = [('Content-type', 'text/html'),
               ('Content-Length', str(len(test)))]
    start_response(status, headers)
    yield test

# mod_wsgi need the *application* variable to serve our small app
application = wsgi_app
