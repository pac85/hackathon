import pymysql.cursors
import json

#savefile = open("/srv/http/savefile.json", 'w')
def save_html(offerID, innerHTML):
    pass

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='vito',
                             db='voli',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

def add_packet(json_request):
    parsed_params = json.loads(json_request)
    try:
        name = parsed_params['name']
    except:
        name="err"
    try:
        userID = parsed_params['userID']
    except:
        userID = "err"

    with connection.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO packetsHead (`name`, `userID`) VALUES (%s, %s)"
        cursor.execute(sql, (name, userID))
    connection.commit()
    with connection.cursor() as cursor:
        sql = "SELECT `ID` FROM packetsHead WHERE 1"
        cursor.execute(sql)
        ID = 0
        for id in cursor.fetchall():
            ID = id['ID']

        for cartElement in parsed_params['cart']:
            offerID = cartElement['offerID']
            qty = cartElement['qty']
            item_type = cartElement['item_type']
            #save_html(offerID, cartElement['innerHTML'])
            with connection.cursor() as cursor:
                # Create a new record
                sql = "INSERT INTO packetItems (`pHeadID`, `itemType`, `itemID`, `itemIDint`, `qty`) VALUES (%s, %s, %s, %s, %s)"
                cursor.execute(sql, (str(ID), str(item_type), str(offerID), str('0'), str(qty)))
            connection.commit()
    return '<h3><center><b style="color:#00FF00">packet added succesfuly</b></center></h3>'.encode('utf-8')
