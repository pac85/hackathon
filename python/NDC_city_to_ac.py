city_to_ac_dict = {}
ac_to_city_dict = {}

#city_to_ac_dict[''] = ''
city_to_ac_dict['Palermo'] = 'PMO'
city_to_ac_dict['Catania'] = 'CTA'
city_to_ac_dict['London'] = 'LHR'
city_to_ac_dict['London/Gatewick'] = 'LGW'
city_to_ac_dict['Madrid'] = 'MAD'
city_to_ac_dict['Milano Linate'] = 'LIN'
city_to_ac_dict['Berlino Tegel'] = 'SXF'
city_to_ac_dict['Barcellona'] = 'BCN'
city_to_ac_dict['Mosca'] = 'MOW'
city_to_ac_dict['Atlanta'] = 'ATL'
city_to_ac_dict['Beijing'] = 'PEK'
city_to_ac_dict['Dubai'] = 'DXB'
city_to_ac_dict['Tokyo'] = 'HND'
city_to_ac_dict['Los Angeles'] = 'LAX'
city_to_ac_dict["O'Hare"] = 'ORD'
city_to_ac_dict['Hong Kong'] = 'HKG'
city_to_ac_dict['Shanghai'] = 'PVG'
city_to_ac_dict['Parigi'] = 'CDG'
city_to_ac_dict['Amsterdam'] = 'AMS'
city_to_ac_dict['Dallas'] = 'DFW'
city_to_ac_dict['Guangzhou'] = 'CAN'
city_to_ac_dict['Francoforte'] = 'FRA'
city_to_ac_dict['Instanbul'] = 'IST'
city_to_ac_dict['Delhi'] = 'DEL'
city_to_ac_dict['Rotterdam'] = 'RTM'
city_to_ac_dict['Singapore'] = 'SIN'
city_to_ac_dict['Denver'] = 'DEN'
city_to_ac_dict['New York'] = 'JFK'
city_to_ac_dict['San Francisco'] = 'SFO'
city_to_ac_dict['Sepang'] = 'KUL'
city_to_ac_dict['Las Vegas'] = 'LAS'
city_to_ac_dict['Toronto'] = 'YYZ'
city_to_ac_dict['Abu Dhabi'] = 'AUH'
city_to_ac_dict['Hollywood'] = 'HWO'
city_to_ac_dict['Rio De Janeiro'] = 'RIO'
city_to_ac_dict['Bucharest'] = 'BUH'
city_to_ac_dict['Buenos Aires'] = 'BUE'
city_to_ac_dict['Chicago'] = 'CHI'
city_to_ac_dict['Seattles'] = 'SEA'
city_to_ac_dict['Giacarta'] = 'JKT'
city_to_ac_dict['Miami'] = 'MIA'
city_to_ac_dict['Ottawa'] = 'YOW'
city_to_ac_dict['Vienna'] = 'VIE'
city_to_ac_dict['Bruxelles'] = 'BRU'
city_to_ac_dict['Atene'] = 'ATH'
city_to_ac_dict['Boston'] = 'BOS'
city_to_ac_dict['Baghdad'] = 'BGV'
city_to_ac_dict['Dublino'] = 'DUB'
city_to_ac_dict['Roma'] = 'FCO'
city_to_ac_dict['Tripoli'] = 'TIP'
city_to_ac_dict['Città Del Messico'] = 'MEX'
city_to_ac_dict['Oslo'] = 'OSL'
city_to_ac_dict['Praga'] = 'PRG'
city_to_ac_dict['Washington'] = 'WAS'
city_to_ac_dict['Dakar'] = 'DKR'
city_to_ac_dict['Belgrado'] = 'BEG'
city_to_ac_dict['Stoccolma'] = 'ARN'
city_to_ac_dict['Manchester'] = 'MAN'
city_to_ac_dict['Budapest'] = 'BUD'
city_to_ac_dict['Bangkok'] = 'BKK'
city_to_ac_dict['Johannesburg'] = 'JNB'
city_to_ac_dict['Medellìn'] = 'MDE'
city_to_ac_dict['Liverpool'] = 'LPL'
city_to_ac_dict['Taipei'] = 'TPE'
city_to_ac_dict['Tehran'] = 'THR'
city_to_ac_dict['Monaco'] = 'MCM'
city_to_ac_dict['Monaco Di Baviera'] = 'MUC'
city_to_ac_dict['Lussemburgo'] = 'LUX'
city_to_ac_dict['Lisbona'] = 'LIS'
city_to_ac_dict['Helsinki'] = 'HEL'
city_to_ac_dict['Reykjavik'] = 'RKV'
city_to_ac_dict['Cairo'] = 'CAI'
city_to_ac_dict['Ibiza'] = 'IBZ'
city_to_ac_dict['Phoenix'] = 'PHX'
city_to_ac_dict['Sacramento'] = 'SMF'
city_to_ac_dict['Austin'] = 'AUS'
city_to_ac_dict['Richmond'] = 'RIC'
city_to_ac_dict['Firenze'] = 'FLR'
city_to_ac_dict['London'] = 'LCY'



for city, ac in city_to_ac_dict.items():
    ac_to_city_dict[ac] = city

def get_airportCode(city_name):
    try:
        return city_to_ac_dict[city_name.capitalize()]
    except KeyError:
        return city_name[:3].upper()


def get_city_name(airportCode):
    try:
        return ac_to_city_dict[airportCode.upper()]
    except KeyError:
        return airportCode[:3].upper()
