from urllib.request import Request, urlopen
#import xml.etree.ElementTree as ET
from lxml import etree as ET
import pymysql.cursors
import json

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='vito',
                             db='voli',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

default_url_str = 'http://live3-wsd-dev.crsengine.com/connectorxml/services'
default_xml_request = open('nrequest.xml', 'r').read()

def send_request(xml_data = default_xml_request, target = default_url_str):
    req =  Request(target, data = xml_data.encode('utf8'), headers = {'Content-Type' : 'text/xml'}, method = 'POST')
    return urlopen(req).read()

class Hotel:
    #HotelCityCode HotelCode HotelName HotelSegmentCategoryCode
    #Poition = {Latitude, Longitude}
    #Address = {AddressLine: ,CityName: ,PostalCode: ,CountryName:}
    #PhoneNumbers = []
    #Rating (provider, rating)
    #ImageURL
    #Emails = []
    #URLs = []

    def __init__(self, HotelCityCode, HotelCode, HotelName, HotelSegmentCategoryCode, Poition, Address, PhoneNumbers, Rating, ImageURL, Emails, URLs):
        self.HotelCityCode = HotelCityCode
        self.HotelCode = HotelCode
        self.HotelName = HotelName
        self.HotelSegmentCategoryCode = HotelSegmentCategoryCode
        self.Poition = Poition
        self.Address = Address
        self.PhoneNumbers = PhoneNumbers
        self.Rating = Rating
        self.ImageURL = ImageURL
        self.Emails = Emails
        self.URLs = URLs

    def get_partial_page(self):
        return """
        <div id="""+"'"+self.HotelCode+"'"+""" style="display:none">
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+"'"+self.ImageURL+"'"+""" style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.HotelName +"""|</b> </h3>
                        <p><pre>"""+str(self.Address)+"""</pre></p>
                    </th>
                    <th>

                    </th>
                </tr>
            </table>
        </div>
        <div>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+"'"+self.ImageURL+"'"+""" style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.HotelName +"""|</b> </h3>
                        <p><pre>"""+str(self.Address)+"""</pre></p>
                    </th>
                    <th>

                    </th>
                </tr>
            </table>
        </div>
        """

class RoomStay:
    #AvailabilityStatus RoomStayCandidateRPH
    def __init__(self, AvailabilityStatus, RoomStayCandidateRPH):
        self.AvailabilityStatus = AvailabilityStatus
        self.RoomStayCandidateRPH = RoomStayCandidateRPH

def parse(xml_code):
    xml_root = ET.fromstring(xml_code)
    if xml_root.tag != 'SOAP-ENV:Envelope' and xml_root.tag != '{http://schemas.xmlsoap.org/soap/envelope/}Envelope':
        print("InfoTech parser error: root is not SOAP-ENV:Envelope, but " + xml_root.tag)
        return

    xml_body = xml_root.find('{http://schemas.xmlsoap.org/soap/envelope/}Body').find('{http://www.opentravel.org/OTA/2003/05}OTA_HotelAvailRS')

    xml_RoomStays = xml_body.find('{http://www.opentravel.org/OTA/2003/05}RoomStays')
    xml_Criteria = xml_body.find('{http://www.opentravel.org/OTA/2003/05}Criteria')

    Hotels = []
    RoomStays = []

    for xml_RoomStay in xml_RoomStays:
        RoomStays.append(RoomStay(xml_RoomStay.attrib['AvailabilityStatus'], xml_RoomStay.attrib['RoomStayCandidateRPH']))
        child = xml_RoomStay.find('{http://www.opentravel.org/OTA/2003/05}BasicPropertyInfo')
        HotelCityCode = child.get('HotelCityCode')
        HotelCode = child.get('HotelCode')
        HotelName = child.get('HotelName')
        HotelSegmentCategoryCode = child.get('HotelSegmentCategoryCode')

        xml_Position = child.find('{http://www.opentravel.org/OTA/2003/05}Position')
        Latitude = xml_Position.get('Latitude')
        Longitude = xml_Position.get('Longitude')

        xml_Address = child.find('{http://www.opentravel.org/OTA/2003/05}Address')
        AddressLine = xml_Address.find('{http://www.opentravel.org/OTA/2003/05}AddressLine').text
        CityName = xml_Address.find('{http://www.opentravel.org/OTA/2003/05}CityName').text
        PostalCode = xml_Address.find('{http://www.opentravel.org/OTA/2003/05}PostalCode').text
        CountryName = xml_Address.find('{http://www.opentravel.org/OTA/2003/05}CountryName').get('Code')

        Numbers = []
        for xml_ContactNumber in child.findall('{http://www.opentravel.org/OTA/2003/05}ContactNumbers'):
            if xml_ContactNumber.tag is '{http://www.opentravel.org/OTA/2003/05}ContactNumber':
                Numbers.append(xml_ContactNumber.get('{http://www.opentravel.org/OTA/2003/05}PhoneNumber'))

        xml_Award = child.find('{http://www.opentravel.org/OTA/2003/05}Award')
        Award = (xml_Award.get('{http://www.opentravel.org/OTA/2003/05}Provider'), xml_Award.get('{http://www.opentravel.org/OTA/2003/05}Rating'))

        xml_TPA_Extensions = xml_RoomStay.find('{http://www.opentravel.org/OTA/2003/05}TPA_Extensions')

        image_URL = xml_TPA_Extensions.find('{http://www.opentravel.org/OTA/2003/05}HotelInfo').find('{http://www.opentravel.org/OTA/2003/05}Descriptions').find('{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptions').find('{http://www.opentravel.org/OTA/2003/05}MultimediaDescription').find('{http://www.opentravel.org/OTA/2003/05}ImageItems').find('{http://www.opentravel.org/OTA/2003/05}ImageItem').find('{http://www.opentravel.org/OTA/2003/05}ImageFormat').find('{http://www.opentravel.org/OTA/2003/05}URL').text
        Emails = []
        URLs = []
        xml_ContactInfo = xml_TPA_Extensions.find('{http://www.opentravel.org/OTA/2003/05}ContactInfos').find('{http://www.opentravel.org/OTA/2003/05}ContactInfo')
        for xml_Email in xml_ContactInfo.find('{http://www.opentravel.org/OTA/2003/05}Emails'):
            Emails.append(xml_Email.text)
        for xml_URL in xml_ContactInfo.find('{http://www.opentravel.org/OTA/2003/05}URLs'):
            URLs.append(xml_URL.text)



        Hotels.append(   Hotel(  HotelCityCode,
                                 HotelCode,
                                 HotelName,
                                 HotelSegmentCategoryCode,
                                 [Latitude, Longitude],
                                 {'AddresLine':AddressLine, 'CityName':CityName, 'PostalCode':PostalCode, 'CountryName':CountryName},
                                 Numbers,
                                 Award,
                                 image_URL,
                                 Emails,
                                 URLs))

    return (Hotels, RoomStays)


def gen_hotel_request(json_reuqest):
    parsed_params = json.loads(json_reuqest)
    city = ''
    rating = 0
    try:
        city = parsed_params['city']
        rating = parsed_params['stars']
    except:
        pass

    #find('{http://schemas.xmlsoap.org/soap/envelope/}Envelope')#
    parsed_request = ET.fromstring(default_xml_request)
    body = parsed_request.find('{http://schemas.xmlsoap.org/soap/envelope/}Body')
    OTA = body.find('{http://www.opentravel.org/OTA/2003/05}OTA_HotelAvailRQ')
    avail = OTA.find('{http://www.opentravel.org/OTA/2003/05}AvailRequestSegments').find('{http://www.opentravel.org/OTA/2003/05}AvailRequestSegment')
    criterion = avail.find('{http://www.opentravel.org/OTA/2003/05}HotelSearchCriteria').find('{http://www.opentravel.org/OTA/2003/05}Criterion')
    criterion.find('{http://www.opentravel.org/OTA/2003/05}Award').set('Rating', str(rating))
    #criterion.find('{http://www.opentravel.org/OTA/2003/05}HotelRef').attrib = {'HotelCityCode':city}
    criterion.find('{http://www.opentravel.org/OTA/2003/05}Award').set('HotelCityCode', city)
    return ET.tostring(parsed_request, encoding='utf8', method='xml').decode('utf-8')


#testHotel, testRoomstay = parse(send_request().decode("utf-8"))
def get_hotel_search_results(json_reuqest):
    hotels, roomstays = parse(send_request(xml_data = gen_hotel_request(json_reuqest)).decode("utf-8"));
    result = '';
    for hotel in hotels:
        result += hotel.get_partial_page();
    return result

class altHotel:
    '''
    toadd.ID = hotel['ID']
    toadd.Nome = hotel['environment']
    toadd.Indirizzo = hotel['Indirizzo']
    toadd.CAP = hotel['CAP']
    toadd.Citta = hotel['Citta']
    toadd.Email = hotel['Email']
    toadd.TipoStruttura = hotel['Tipo Struttura']
    toadd.NumeroTelefono = hotel['Numero Telefono']
    toadd.NumeroStelle = hotel['Numero Stelle']
    toadd.Immagine = hotel['Immagine']
    '''
    def __init__(self,
    ID,
    Nome,
    Indirizzo,
    CAP,
    Citta,
    Email,
    TipoStruttura,
    NumeroTelefono,
    NumeroStelle,
    Immagine):
        self.ID = ID
        self.Nome = Nome
        self.Indirizzo = Indirizzo
        self.CAP = CAP
        self.Citta = Citta
        self.Email = Email
        self.TipoStruttura = TipoStruttura
        self.NumeroTelefono = NumeroTelefono
        self.NumeroStelle = NumeroStelle
        self.Immagine = Immagine

    def get_partial_page(self):
        return """
        <div id="""+"'"+str(self.ID)+"_hotel'"+""" style="display:none">
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+"'immaginiHotel/"+self.Immagine+"'"+""" style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.Nome +"""|</b> </h3>
                        <p><pre>"""+self.Indirizzo+"""</pre></p>
                    </th>
                    <th>
                        <input id="""+"\""+str(str(self.ID))+"_hotel_cart_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                        <button class="w3-button w3-dark-grey" onclick="updateItemQty("""+"'"+str(self.ID)+"_hotel'"+""");" >update</button>
                        </br>
                        <button class="w3-button w3-dark-grey" onclick="removeItem("""+"'"+str(self.ID)+"_hotel'"+""");" >X</button>
                    </th>
                </tr>
            </table>
        </div>
        <div>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+"'immaginiHotel/"+self.Immagine+"'"+""" style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.Nome +"""|</b> </h3>
                        <p><pre>"""+self.Indirizzo+"""</pre></p>
                    </th>
                    <th>
                        <input id="""+"\""+str(self.ID)+"_hotel_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                        </br>
                        <button class="w3-button w3-dark-grey" onclick="addHotelToCart_pre("""+"'"+str(self.ID)+"_hotel'"+""");" >add to cart</button>
                    </th>
                </tr>
            </table>
        </div>
        """


def pull_hotels(json_reuqest):
    parsed_params = json.loads(json_reuqest)
    city = ''
    rating = 0
    try:
        city = parsed_params['city']
        rating = parsed_params['stars']
    except:
        pass

    with connection.cursor() as cursor:
        sql = "SELECT * FROM `Hotel` WHERE Citta = %s AND NumeroStelle = %s"
        cursor.execute(sql, (city, rating))

        means = []
        for hotel in cursor.fetchall():
            toadd = altHotel(
            hotel['ID'],
            hotel['Nome'],
            hotel['Indirizzo'],
            hotel['CAP'],
            hotel['Citta'],
            hotel['Email'],
            hotel['Tipo struttura'],
            hotel['Numero telefono'],
            hotel['NumeroStelle'],
            hotel['Immagine'])
            '''toadd.ID = hotel['ID']
            toadd.Nome = hotel['Nome']
            toadd.Indirizzo = hotel['Indirizzo']
            toadd.CAP = hotel['CAP']
            toadd.Citta = hotel['Citta']
            toadd.Email = hotel['Email']
            toadd.TipoStruttura = hotel['Tipo struttura']
            toadd.NumeroTelefono = hotel['Numero telefono']
            toadd.NumeroStelle = hotel['Numero Stelle']
            toadd.Immagine = hotel['Immagine']'''
            means.append(toadd)

        return means

def alt_get_hotel_search_results(json_reuqest):
    result = ''
    for hotel in pull_hotels(json_reuqest):
        result += hotel.get_partial_page()
    return result.encode('utf-8')
