import pymysql.cursors
import json

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='vito',
                             db='voli',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

class Mean:
    def __init__(self,
    ID,
    Colore,
    Nome,
    Carburante,
    Modello,
    Marca,
    NumeroPorte,
    Stelle):
        self.ID = ID
        self.Colore = Colore
        self.Nome = Nome
        self.Carburante = Carburante
        self.Modello = Modello
        self.Marca = Marca
        self.NumeroPorte = NumeroPorte
        self.Stelle = Stelle

    def partial_page(self):
        return """
        <div id="""+"'"+str(self.ID)+"_mean'"+""" style="display:none">
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+'"meansImages/'+self.Nome+'.png"'+"""style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.Nome +"""|</b> </h3>
                    </th>
                    <th>
                        <input id="""+"\""+str(self.ID)+"_mean_cart_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                        <button class="w3-button w3-dark-grey" onclick="updateItemQty("""+"'"+str(self.ID)+"_mean'"+""");" >update</button>
                        </br>
                        <button class="w3-button w3-dark-grey" onclick="removeItem("""+"'"+str(self.ID)+"_mean'"+""");" >X</button>
                    </th>
                </tr>
            </table>
        </div>
        <div>
            <table style="width:100%">
              <tr>
                <th style="width:5%"><img src="""+'"meansImages/'+self.Nome+'.png"'+"""style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                <th style="width:90%">
                    <h3><b style="color:#E65940">|"""+ self.Nome +"""|</b> </h3>
                </th>
                <th>
                    <input id="""+"\""+str(self.ID)+"_mean_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                    </br>
                    <button class="w3-button w3-dark-grey" onclick="addMeanToCart_pre("""+"'"+str(self.ID)+"_mean'"+""");" >add to cart</button>
                </th>
              </tr>
            </table>
        </div> """


def pull_means(json_reuqest):
    parsed_request = json.loads(json_reuqest)
    Citta = parsed_request['Citta']
    Tipo = parsed_request['Tipo']
    Stelle = parsed_request['Stelle']
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `Mezzi` WHERE Citta = %s AND Tipo = %s AND Stelle = %s"
        cursor.execute(sql, (str(Citta), str(Tipo), Stelle))

        means = []
        for mean in cursor.fetchall():
            means.append(
            Mean(
            mean['ID'],
            mean['Colore'],
            mean['Nome'],
            mean['Carburante'],
            mean['Modello'],
            mean['Marca'],
            mean['Numero porte'],
            mean['Stelle']
            ))

        return means

def get_means_search_results(json_reuqest):
    resoult = ''
    for mean in pull_means(json_reuqest):
        resoult += mean.partial_page()
    return resoult.encode('utf-8')
