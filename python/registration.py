import pymysql.cursors
import json

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='vito',
                             db='voli',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


class userBaseData:
    def __init__(self,
    ID,
    name,
    lastname,
    e_mail,
    password):
        self.ID = ID
        self.name = name
        self.lastname = lastname
        self.e_mail = e_mail
        self.password = password
    def to_tuple(self):
        return (self.ID, self.name, self.lastname, self.e_mail, self.password)

def push_users(json_request):
    parsed_params = json.loads(json_request)
    ubd = userBaseData(
    0,
    parsed_params['name'],
    parsed_params['lastname'],
    parsed_params['e_mail'],
    parsed_params['password'])
    #try:
    with connection.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO User (`name`, `lastname`, `e_mail`, `password`) VALUES (%s, %s, %s, %s)"
        cursor.execute(sql, ubd.to_tuple()[1:])
    connection.commit()
    '''except:
        return """
        <h3><center><b style="color:#E65940">We are sorry but an error occurred, please ensure the data you inserted is valid</b></center></h3>
        """.encode('utf-8')'''
    return """<h3><center><b style="color:#00FF00">The registration was succesful</b></center></h3>""".encode('utf-8')
