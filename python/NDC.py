import requests
import json
import pymysql.cursors
from NDC_city_to_ac import *

class FlightSegment:
    #offerID
    #totalPrice
    #currency
    #segmentID
    #ODRef
    #originDestination  {departure:{airportCode:, time:, airportName:, terminalName:, date:},
    #                       arrival:{airportCode:, time:, airportName:, terminalName:, date:}}
    #airlineID
    #marketing_name
    #flightNumber
    #aircraftCode
    #aircraft_name
    #flightDuration
    #class_refs
    #class_code
    #class_marketingName
    #class_disclosures
    #seatsLeft
    #stopQuantity
    def __init__(self,
                 offerID,
                 currency,
                 totalPrice,
                 segmentID,
                 ODRef,
                 originDestination,
                 airlineID,
                 marketing_name,
                 flightNumber,
                 aircraftCode,
                 aircraft_name,
                 flightDuration,
                 class_refs,
                 class_code,
                 class_marketingName,
                 class_disclosures,
                 seatsLeft,
                 stopQuantity):
        self.offerID = offerID
        self.currency   = currency
        self.totalPrice =   totalPrice
        self.segmentID   = segmentID
        self.ODRef = ODRef
        self.originDestination = originDestination
        self.airlineID = airlineID
        self.marketing_name =   marketing_name
        self.flightNumber   = flightNumber
        self.aircraftCode   = aircraftCode
        self.aircraft_name = aircraft_name
        self.flightDuration =   flightDuration
        self.class_refs =   class_refs
        self.class_code =   class_code
        self.class_marketingName = class_marketingName
        self.class_disclosures = class_disclosures
        self.seatsLeft = seatsLeft
        self.stopQuantity   = stopQuantity

    def insert_in_db(self,  db_connection):
        sql =   "INSERT INTO 'flightSegment' ('offerID', 'currency', 'totalPrice', 'segmentID', 'ODRef') VALUES(%s, %s, %d, %s, %s)"
        with db_connection.cursor() as cursor:
            cursor.execute(sql, (self.offerID, self.currency, self.totalPrice, self.segmentID, self.ODRef))
        db_connection.commit()

    def get_partial_page(self):
        return """
        <div id="""+"'"+self.offerID+"'"+""" style="display:none">
            <table style="width:100%">
                <tr>
                    <th style="width:5%"><img src="""+'"logos/'+self.marketing_name+'.png"'+"""style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                    </th>
                    <th style="width:90%">
                        <h3><b style="color:#E65940">|"""+ self.marketing_name +"""|</b> """+get_city_name(self.originDestination['departure']['airportCode'])+"-"+get_city_name(self.originDestination['arrival']['airportCode'])+"""</h3>
                            <div id="progressbar">
                                <div><center>"""+self.flightDuration+"""ore</center></div>
                            </div>
                        <p><pre><span style="color:#E65940">Partenza:</span>"""+self.originDestination['departure']['date'] + ' ' +self.originDestination['departure']['time'] + """          <span style="color:#E65940">Arrivo:</span>"""+ self.originDestination['departure']['date'] + ' ' +self.originDestination['departure']['time'] +"""</pre></p>
                    </th>
                    <th>
                        <input id="""+"\""+self.offerID+"_cart_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                        <button class="w3-button w3-dark-grey" onclick="updateFlightQty("""+"'"+self.offerID+"'"+""");" >update</button>
                        </br>
                        <button class="w3-button w3-dark-grey" onclick="removeFlight("""+"'"+self.offerID+"'"+""");" >X</button>
                    </th>
                </tr>
            </table>
        </div>
        <div>
            <table style="width:100%">
              <tr>
                <th style="width:5%"><img src="""+'"logos/'+self.marketing_name+'.png"'+"""style="border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 150px; height:150px;"></img></th>
                <th style="width:90%">
                    <h3><b style="color:#E65940">|"""+ self.marketing_name +"""|</b> """+get_city_name(self.originDestination['departure']['airportCode'])+"-"+get_city_name(self.originDestination['arrival']['airportCode'])+"""</h3>
                        <div id="progressbar">
                            <div><center>"""+self.flightDuration+"""ore</center></div>
                        </div>
                    <p><pre><span style="color:#E65940">Partenza:</span>"""+self.originDestination['departure']['date'] + ' ' +self.originDestination['departure']['time'] + """          <span style="color:#E65940">Arrivo:</span>"""+ self.originDestination['departure']['date'] + ' ' +self.originDestination['departure']['time'] +"""</pre></p>
                </th>
                <th>
                    <input id="""+"\""+self.offerID+"_qty\""+""" type="number" name="quantity" min="1" max="5"></input>
                    </br>
                    <button class="w3-button w3-dark-grey" onclick="addFlightToCart_pre("""+"'"+self.offerID+"'"+""");" >add to cart</button>
                </th>
              </tr>
            </table>
        </div> """


def get_json_code(payload, url):

    headers = {
    'content-type': "application/json",
    'ag-providers': "*",
    'authorization': "504429bc410b5a355cd9bc53f9cb3d67",
    'cache-control': "no-cache",
    'postman-token': "3d1f1290-76d4-788a-993e-08b427725581"
    }

    return  requests.request("POST", url, data=payload, headers=headers)

#used as default argument for the db connection
'''default_dbconn = pymysql.connect(   host='127.0.0.1',
                                    user='root',
                                    password='vito',
                                    db='voli',
                                    charset='utf8_general_ci',
                                    cursorclass=pymysql.cursors.DictCursor)

#does the request to the ndc server and stores the data in the SQL db
def copy_to_db(request_payload, url = "https://proxy.airgateway.net/v1/AirShopping", dbconn = default_dbconn):
    parsed_response = json.loads(get_json_code(request_payload), url)
    for result  in parsed_response['result']:
        offerID =   result['offerID']
        totalPrice = result['price']['totalPrice']
        currency = result['price']['currency']
        for flightSegment   in result['flightSegments']:
            segmentID = flightSegment['segmentID']
            ODRef = flightSegment['ODRef']

            departure = ['originDestination']['departure']
            arrival = ['originDestination']['arrival']'''

def parse_flightSegments(NDCresponse):
    flightSegments  = []
    parsed_response = json.loads(NDCresponse)
    for result  in parsed_response['result']:
        offerID =   result['offerID']
        price   = result['price']
        for flightSegment   in result['flightSegments']:
            ODRef = flightSegment['ODRef']
            try:
                segmentID = flightSegment['segmentID']
            except KeyError:
                segmentID = ''
            aircraftCode    = flightSegment['equipment']['aircraftCode']
            aircraft_name = flightSegment['equipment']['name']
            class_code =    flightSegment['flightDetail']['classOfService']['code']
            class_refs =    flightSegment['flightDetail']['classOfService']['refs']
            try:
                seatsLeft = flightSegment['flightDetail']['seatsLeft']
            except KeyError:
                seatsLeft = ''
            flightDuration =    flightSegment['flightDetail']['flightDuration']
            try:
                stopsQuantity = flightSegment['flightDetail']['stopsQuantity']
            except KeyError:
                stopsQuantity =  ''
            class_marketingName = flightSegment['flightDetail']['classOfService']['marketingName']
            try:
                class_disclosures = flightSegment['flightDetail']['classOfService'][' disclosures ']
            except KeyError:
                class_disclosures =  []
            marketing_name =    flightSegment['marketingCarrier']['name']
            airlineID = flightSegment['marketingCarrier']['airlineID']
            flightNumber    = flightSegment['marketingCarrier']['flightNumber']
            try:
                originDestination = flightSegment['originDestination']
            except KeyError:
                originDestination =  {}
                print('NDC parser error: couldn t find originDestination')

            flightSegments.append(FlightSegment(offerID,
                                                price['currency'],
                                                price['totalPrice'],
                                                segmentID,
                                                ODRef,
                                                originDestination,
                                                airlineID,
                                                marketing_name,
                                                flightNumber,
                                                aircraftCode,
                                                aircraft_name,
                                                flightDuration,
                                                class_refs,
                                                class_code,
                                                class_marketingName,
                                                class_disclosures,
                                                seatsLeft,
                                                stopsQuantity))

    return flightSegments

def gen_request(json_params):
    parsed_params = json.loads(json_params)
    parsed_request = json.loads(open('request.json', 'r').read())

    is_oneway = parsed_params['is_oneway']

    parsed_request['originDestinations'][0]['departure']['airportCode'] = get_airportCode(parsed_params['oneway_departure'])
    parsed_request['originDestinations'][0]['arrival']['airportCode'] = get_airportCode(parsed_params['oneway_arrival'])

    if is_oneway:
        parsed_request['originDestinations'] = parsed_request['originDestinations'][:1]
    else:
        parsed_request['originDestinations'][1]['departure']['airportCode'] = get_airportCode(parsed_params['roundtrip_departure'])
        parsed_request['originDestinations'][1]['arrival']['airportCode'] = get_airportCode(parsed_params['roundtrip_arrival'])

    return json.dumps(parsed_request)

def get_flights_search_results(request, url, start = 0, end = 0):
    results = ''
    try:
        parsed_responses = parse_flightSegments(get_json_code(request, url).text)
    except:
        return """
        <h3><center><b style="color:#E65940">We are sorry but we couldn't find the flight you are looking for</b></center></h3>
        """
    start = max(0, start)
    if end == 0:
        end = len(parsed_responses)
    end = min(len(parsed_responses), end)
    for parsed_response in parsed_responses[start:end]:
        results += parsed_response.get_partial_page()
    return results


#test_request = open('request.json', 'r').read()
#print(len(parse_flightSegments(get_json_code(test_request, "https://proxy.airgateway.net/v1/AirShopping").text)))
